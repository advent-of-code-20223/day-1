use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    let file = File::open("input.txt").unwrap();
    let reader = BufReader::new(file);
    let mut results: Vec<u32> = Vec::new();
    let mut sum: u32 = 0;
    for line in reader.lines() {
        let l = line.unwrap();
        if l.is_empty() {
            let pos = results.binary_search(&sum).unwrap_or_else(|e| e);
            results.insert(pos, sum);
            sum = 0;
        }
        else {
            let n: u32 = l.trim().parse().unwrap();
            sum += n;
        }
    }
    println!("{}", results[results.len() - 1] + results[results.len() - 2] + results[results.len() - 3]);
}
